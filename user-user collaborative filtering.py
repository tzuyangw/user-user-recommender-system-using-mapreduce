from pyspark import SparkContext, SparkConf
import math

TARGET_ID = 1
TOP_NUM = 50

Rating_List = []
User_Num = 0

def data_map(line):
    maplist = []
    elements = line.split("\t")
    maplist.append((int(elements[0]), int(elements[1]), float(elements[2])))
    return maplist

def item_list():
    global data, Rating_List, User_Num
    elements = data.collect()
    current = elements[0][0][0]
    temp = []
    for element in elements:
        if(current == element[0][0]):
            temp.append((element[0][1], element[0][2]))
        else:
            Rating_List.append(temp)
            User_Num += 1
            temp = []
            current = element[0][0]
            temp.append((element[0][1], element[0][2]))

    User_Num += 1
    Rating_List.append(temp)

def difference_value(rating1, rating2):
    return rating1 - rating2


def pearson_correlation_coefficient():
    global Rating_List, average_rating
    similarity_list = []
    average = average_rating.collect()
    target_list = Rating_List[TARGET_ID - 1]
    target_average = average[TARGET_ID - 1][1]

    for i in range(0, User_Num):
        if(i == TARGET_ID - 1):
            continue
        else:
            numerator = 0
            t1_denominator = 0
            t2_denominator = 0
            denominator = 0
            similarity = 0
            for item1 in target_list:
                neighbor_average = average[i][1]
                for item2 in Rating_List[i]:

                    # If they have rated the same movie
                    if(item1[0] == item2[0]):

                        # Calculate the differemve between two ratings
                        dif1 = difference_value(item1[1], target_average)
                        dif2 = difference_value(item2[1], neighbor_average)
                        
                        # Add the result of multiplication into numberator
                        numerator += dif1 * dif2

                        # Calculate the value of square of the difference
                        t1_denominator += dif1 ** 2
                        t2_denominator += dif2 ** 2

            # Calculate the denominator of the similarity
            denominator = math.sqrt(t1_denominator) * math.sqrt(t2_denominator)
            
            # Calculate the similarity between two user and store them in the format (similarity, target user, beighbor user)
            if(denominator != 0):
                similarity = numerator / denominator
            similarity_list.append((similarity, TARGET_ID, i + 1))

    return similarity_list

def movie_map():
    global Rating_List, similarity_list
    maplist = []
    for i in range(TOP_NUM):
        for movie in Rating_List[similarity_list[i][2]]:
            maplist.append((movie[0], (movie[1], similarity_list[i][0])))
    return maplist

def movie_filter_map():
    global movie_list, movie_count
    temp = movie_list.collect()
    maplist = []
    for movie1 in movie_count:
        for movie2 in temp:
            if(movie1[0] == movie2[0]):
                maplist.append(movie2)

    return maplist


conf = SparkConf().setMaster("local").setAppName("term_project")
sc = SparkContext(conf = conf)

# Read the data from data.txt and sotore them by the format of (user id, movie id, rating)
data = sc.textFile("data.txt").map(data_map)

### Calculat the average rating of each user ###
print("......Calculate average rating......")

# First calculate the total number of rating for each user. (user_id, 1) => reduce => (user_id, number of rating)
rating_num = data.map(lambda x: (x[0][0], 1)).reduceByKey(lambda x, y: x + y)

# Then calculate the total rating score for each user. (user_id, total_rating)
total_rating = data.map(lambda x: (x[0][0], x[0][2])).reduceByKey(lambda x, y: x + y)

# Union previous info and reduce them by formula y / x => (user_id, average_rating)
average_rating = rating_num.union(total_rating).reduceByKey(lambda x, y: y / x)
print("......Done......")

### Find common rating of target user###
print("......Calculate similarity for user " + str(TARGET_ID) + "......")

# Find the rating list for each user. => [(item_id, rating), ... ,(item_id, rating)]
item_list()

# Calculate the prearson correlation coefficient between target and neighbor
similarity_list = pearson_correlation_coefficient()
similarity_list.sort(reverse = True)

fp = open("similarity.txt", "w")
for element in similarity_list:
    fp.writelines(str(element[1]) + "   " + str(element[2]) + "   " + str(element[0]) + '\n')
print("......Done......")

### Find the top 10 recommendation movie ###
print("......Recommend Movie......")
# Only reserve top 50 similarity users
similarity_list = similarity_list[0:TOP_NUM]
movie_list = sc.parallelize(movie_map())

# Count the movie that being rated commonly and reserve the top 30 movie
movie_count = movie_list.map(lambda x: (x[0], 1)).reduceByKey(lambda x, y: x + y)
# (movie id, total number of rating)
movie_count = movie_count.sortBy(lambda x: x[1], ascending=False).collect()[0:30]

# Filter out the movi that not in the top 30 movie list
movie_list_filtered = sc.parallelize(movie_filter_map())

# Calculate the total similarity between target user and neighrbors
total_similarity = movie_list_filtered.map(lambda x: (x[0], x[1][1])).reduceByKey(lambda x, y: x + y)

# Calculate the rating of (rating * similarity)
total_rating = movie_list_filtered.map(lambda x: (x[0], x[1][0] * x[1][1])).reduceByKey(lambda x, y: x + y)

# Top 30 Recommend movie
recommend_movie = total_similarity.union(total_rating).reduceByKey(lambda x, y: y / x)

fp = open("recommend.txt", "w")
for movie in recommend_movie.collect():
    fp.writelines(str(movie[0]) + "    " + str(movie[1]) + '\n')

print("......Done......")
